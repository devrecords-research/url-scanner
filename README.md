# url_scanner

scanner for url links

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.io/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.io/docs/cookbook)

For help getting started with Flutter, view our 
[online documentation](https://flutter.io/docs), which offers tutorials, 
samples, guidance on mobile development, and a full API reference.


## How to start project:
-  Download Android Studio
-  Download Flutter SDK: https://flutter.dev/docs/get-started/install
-  Open project via Android Studio
-  Go to Preferences - Language - Flutter - and link sdk location
-  Add new android emulator
-  Run app on it
