import 'dart:io';
import 'dart:isolate';
import 'package:image/image.dart' as Img;

class DecodeParam {
  final File file;
  final SendPort sendPort;
  DecodeParam(this.file, this.sendPort);
}

void decode(DecodeParam param){
  Img.Image image = Img.decodeImage(param.file.readAsBytesSync());
  Img.Image processImage = Img.brightness(image, 90);
  processImage = Img.contrast(processImage, 140);
  param.sendPort.send(processImage);
}

Future<bool> process(filePath) async {
  ReceivePort receivePort = new ReceivePort();

  await Isolate.spawn(decode,
    new DecodeParam(new File(filePath), receivePort.sendPort));

  Img.Image image = await receivePort.first;
  new File(filePath).writeAsBytesSync(Img.encodeJpg(image));
  return true;
}

