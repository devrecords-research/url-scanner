import 'dart:io';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter/material.dart';
import 'urls.dart';
import 'file_service.dart';
import 'image_processing.dart' as ImageProcessor;
import 'url_item.dart';

class ImageDetail extends StatefulWidget {
  ImageDetail(this.filePath);
  final String filePath;

  @override
  _ImageDetailState createState() => new _ImageDetailState(filePath);
}

class _ImageDetailState extends State<ImageDetail> with SingleTickerProviderStateMixin{
  _ImageDetailState(this.filePath);
  var _done = false;
  List<String> _cachedData = [];
  List<String> _urls = [];
  var _counter = 0;

  final String filePath;

  String recognizedText = "Loading...";

  AnimationController _controller;
  Animation _animation;

  final List<TextEditingController> _controllerList = List();

  FocusNode _focusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    ImageProcessor.process(filePath).then((result){
      setState((){
        _done = result;
      });
    });
    FileService().loadData().then((onValue){
      _cachedData = onValue;
    });
    _initializeVision();


    _controller = AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    _animation = Tween(begin: 300.0, end: 50.0).animate(_controller)
      ..addListener(() {
        setState(() {});
      });

    _focusNode.addListener(() {
      if (_focusNode.hasFocus) {
        _controller.forward();
      } else {
        _controller.reverse();
      }
    });

  }

  @override
  void dispose() {
    _controller.dispose();
    _focusNode.dispose();
    _saveData(_urls, _cachedData , _counter);

    _controllerList.forEach((controller) => controller.dispose());

    super.dispose();
  }

  void _addControllers(){
    for(var i in _urls){
      TextEditingController _controller = new TextEditingController();
      _controller.text = i;
      _controllerList.add(_controller);

    }
  }


  void _initializeVision() async {

    final File imageFile = File(filePath);

    final FirebaseVisionImage visionImage =
        FirebaseVisionImage.fromFile(imageFile);

    final TextRecognizer textRecognizer =
        FirebaseVision.instance.textRecognizer();

    final VisionText visionText =
        await textRecognizer.processImage(visionImage);

    String urlPattern =
        r"http";
    RegExp regExp = RegExp(urlPattern);

    String urlLink =
        "No URL found";

    _urls = [];
    _counter = 0;

    for (TextBlock block in visionText.blocks) {
      for (TextLine line in block.lines){
        if (regExp.hasMatch(line.text)){
           _urls.add(line.text);
           _counter++;
        }
      }
    }
    _addControllers();
    if (this.mounted){
      setState(() {
        recognizedText = urlLink;
      });
    }



  }

  _saveData(List<String> urls, List<String> _cashedData, int counter){
    if (_cashedData == null) {
      _cashedData = [];
    }
    _cashedData = _cashedData + urls;
    if(_cashedData.length <= 10){
      FileService().storeData(_cashedData);
    }else{
      FileService().storeData(_cashedData.sublist(0+counter,10+counter));
    }
  }
  @override
  Widget build(BuildContext context) {
    if(!_done){
      return Container(
        height: 64.0,
        width: 64.0,
        alignment: Alignment.center,
        child: SizedBox(
          width: 64.0,
          height: 64.0,
          child: CircularProgressIndicator(
            value: null,
            valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
          ),
        ),
      );
    }

    return Scaffold(
        appBar: AppBar(title: Text("Taken Photo")),
        resizeToAvoidBottomPadding: true,
        body: InkWell(
          splashColor: Colors.transparent,
          onTap: () {
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: _animation.value,
                child: Container(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 40.0),
                    child: new Container(
                      height: 300,
                      child: Center(child: Image.file(File(filePath))),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Urls(_urls, _focusNode, _controllerList),
              ),
            ],
          ),
        ));
  }
}