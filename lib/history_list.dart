import 'package:flutter/material.dart';
import 'file_service.dart';
import 'urls.dart';
import 'url_item.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HistoryList extends StatefulWidget {
  @override
  _HistoryListState createState() => new _HistoryListState();
}

class _HistoryListState extends State<HistoryList>
    with SingleTickerProviderStateMixin {
  List<String> _urls = [];
  FileService fileService = new FileService();

  AnimationController _controller;
  Animation _animation;

  final List<TextEditingController> _controllerList = List();

  FocusNode _focusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    _loadData();
    _controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    _animation = Tween(begin: 300.0, end: 50.0).animate(_controller)
      ..addListener(() {
        setState(() {});
      });

    _focusNode.addListener(() {
      if (_focusNode.hasFocus) {
        _controller.forward();
      } else {
        _controller.reverse();
      }
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    _focusNode.dispose();
    _controllerList.forEach((controller) => controller.dispose());

    super.dispose();
  }

  void _addControllers() {
    for (var i in _urls) {
      TextEditingController _controller = new TextEditingController();
      _controller.text = i;
      _controllerList.add(_controller);
    }
  }

  _loadData() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      _urls = prefs.getStringList('urls');
      _urls = _urls.reversed.toList();
      _addControllers();
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_urls.length > 0) {
      return Scaffold(
          appBar: AppBar(title: Text("History")),
          resizeToAvoidBottomPadding: false,
          body: InkWell(
            splashColor: Colors.transparent,
            onTap: () {
              FocusScope.of(context).requestFocus(FocusNode());
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: UrlsItem(urls: _urls),
                ),
              ],
            ),
          ));
    } else {
      return Container(
        height: 64.0,
        width: 64.0,
        alignment: Alignment.center,
        child: SizedBox(
          width: 64.0,
          height: 64.0,
          child: CircularProgressIndicator(
            value: null,
            valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
          ),
        ),
      );
    }
  }
}
