import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:share/share.dart';

class Urls extends StatelessWidget {
  final List<String> urls;
  final FocusNode _focusNode;
  final List<TextEditingController> _controllerList;

  Urls(this.urls, this._focusNode, this._controllerList);

  _copyToClipboard(String url, BuildContext context) {
    Clipboard.setData(new ClipboardData(text: url));
    Scaffold.of(context).showSnackBar(SnackBar(content: Text(url)));
  }

  Widget _buildUrlItem(BuildContext context, int index) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 15.0),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                controller: _controllerList[index],
                onChanged: (text) {
                  urls[index] = text;
                },
                focusNode: _focusNode,
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(left: 10),
                  child: new RaisedButton(
                      padding: const EdgeInsets.all(8.0),
                      textColor: Colors.white,
                      color: Colors.white70,
                      onPressed: () {
                        _copyToClipboard(_controllerList[index].text, context);
                      },
                      child: new Icon(
                        Icons.content_copy,
                        color: Colors.blueAccent,
                      )),
                ),
                Padding(
                  padding: EdgeInsets.only(right: 10),
                  child: new RaisedButton(
                    padding: const EdgeInsets.all(8.0),
                    textColor: Colors.white,
                    color: Colors.white70,
                    onPressed: () {
                      Share.share(urls[index]);
                    },
                    child: new Icon(
                      Icons.share,
                      color: Colors.orange,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new ListView.builder(
      itemBuilder: _buildUrlItem,
      itemCount: urls.length,
      physics: AlwaysScrollableScrollPhysics(),
    );
  }
}
