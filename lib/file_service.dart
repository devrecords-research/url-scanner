import 'package:shared_preferences/shared_preferences.dart';

class FileService {
  Future<List<String>>loadData() async {
    final prefs = await SharedPreferences.getInstance();
    List<String> dataList = new List<String>();
    dataList = prefs.getStringList('urls');
    return dataList;
  }

  storeData(List<String> data) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setStringList('urls', data);
  }
}