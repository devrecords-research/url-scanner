import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'image_detail.dart';
import 'history_list.dart';


List<CameraDescription> cameras;

Future<void> main() async {
  cameras = await availableCameras();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'URL Scanner',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'URL Scanner'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  CameraController _controller;


  @override
  void initState(){
    super.initState();

    _controller = CameraController(cameras[0], ResolutionPreset.high);
    _controller.initialize().then((_){
      if (!mounted) {
        return;
      }
      setState(() {});
    });
  }

  @override
  void dispose(){
    _controller.dispose();
    super.dispose();
  }

  void _takePicturePressed() {
    _takePicture().then((String filePath) {
      if (mounted) {
        Navigator.push(context, MaterialPageRoute(builder: (context) => ImageDetail(filePath)));
      }
    });
  }

  void _goToHistory(){
    if(mounted){
      Navigator.push(context, MaterialPageRoute(builder: (context) => HistoryList()));
    }
  }

  String timestamp() => DateTime.now().millisecondsSinceEpoch.toString();

  Future<String> _takePicture() async{
    if(!_controller.value.isInitialized){
      print("Controller is not initialized");
      return null;
    }

    final Directory extDir = await getApplicationDocumentsDirectory();
    final String photoDir = '${extDir.path}/Photos/image_test';
    await Directory(photoDir).create(recursive: true);
    final String filePath = '$photoDir/${timestamp()}.jpg';

    if (_controller.value.isTakingPicture){
      print("Taking picture...");
      return null;
    }

    try {
      await _controller.takePicture(filePath);
    } on CameraException catch (e) {
      print("camera exception occured: $e");
      return null;
    }

    return filePath;
  }



  @override
  Widget build(BuildContext context) {
    if(!_controller.value.isInitialized){
      return Center(child: Text(""));
    }
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            ListTile(
              title: Text("History"),
              trailing: Icon(Icons.arrow_forward),
              onTap: _goToHistory,
            )
          ],
        ),
      ),
      body: Container(
        decoration: BoxDecoration(
          color: Colors.black
        ),
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              AspectRatio(
                aspectRatio: _controller.value.aspectRatio,
                child: CameraPreview(_controller),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 15),
                      child: FlatButton(
                        onPressed: () {
                          _takePicturePressed();
                        },
                        child: const Icon(
                          Icons.camera,
                          color: Colors.white70,
                          size: 50.0,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
