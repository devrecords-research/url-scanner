import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:share/share.dart';
import 'file_service.dart';

class UrlsItem extends StatefulWidget {
  UrlsItem({Key key, @required this.urls}) : super(key: key);
  final List<String> urls;


  @override
  _UrlsItemState createState() => new _UrlsItemState(urls);
}

class _UrlsItemState extends State<UrlsItem> with SingleTickerProviderStateMixin{
  List<String> urls;
  final List<TextEditingController> _controllerList = List();
  FocusNode _focusNode = FocusNode();
  FileService fileService =  FileService();
  AnimationController _controller;
  Animation _animation;

  _UrlsItemState(this.urls);

  @override
  void initState(){
    super.initState();
    _addControllers();
    _controller = AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    _animation = Tween(begin: 300.0, end: 50.0).animate(_controller)
      ..addListener(() {
        setState(() {});
      });


    _focusNode.addListener(() {
      if (_focusNode.hasFocus) {
        _controller.forward();
      } else {
        _controller.reverse();
      }
    });
  }

  @override
  void dispose() {

    _controller.dispose();
    _focusNode.dispose();
    var reversedList = urls.reversed.toList();
    fileService.storeData(reversedList);
    _controllerList.forEach((controller) => controller.dispose());

    super.dispose();
  }

  Widget _buildUrlItem(BuildContext context, int index) {
    return Dismissible(
      background: stackBehindDismiss(),
      key: ObjectKey(urls[index]),
      child: Card(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 15.0),
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  controller: _controllerList[index],
                  onChanged: (text) {
                    urls[index] = text;
                  },
                  focusNode: _focusNode,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  new RaisedButton(
                      padding: const EdgeInsets.all(8.0),
                      textColor: Colors.white,
                      color: Colors.white70,
                      onPressed: () {
                        _copyToClipboard(_controllerList[index].text, context);
                      },
                      child: new Icon(
                        Icons.content_copy,
                        color: Colors.blueAccent,
                      )),
                  new RaisedButton(
                    padding: const EdgeInsets.all(8.0),
                    textColor: Colors.white,
                    color: Colors.white70,
                    onPressed: () {
                      Share.share(urls[index]);
                    },
                    child: new Icon(
                      Icons.share,
                      color: Colors.orange,
                    ),
                  ),
                  new RaisedButton(
                    padding: const EdgeInsets.all(8.0),
                    textColor: Colors.white,
                    color: Colors.white70,
                    onPressed: () {
                      deleteItem(index);
                    },
                    child: new Icon(
                      Icons.delete_outline,
                      color: Colors.redAccent,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
      onDismissed: (direction) {
        deleteItem(index);
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text("Item deleted"),
          ));
      },
    );
  }

  void deleteItem(index){
    setState((){
      urls.removeAt(index);
      _controllerList.removeAt(index);
    });
  }


  @override
  Widget build(BuildContext context) {
    return new ListView.builder(
      itemBuilder: _buildUrlItem,
      itemCount: urls.length,
      physics: AlwaysScrollableScrollPhysics(),
    );
  }

  Widget stackBehindDismiss(){
    return Container(
      alignment: Alignment.centerRight,
      padding: EdgeInsets.only(right: 20.0),
      color: Colors.red,
      child: Icon(
        Icons.delete,
        color: Colors.white,
      ),
    );
  }

  void _addControllers() {
    for (var i in urls) {
      TextEditingController _controller = new TextEditingController();
      _controller.text = i;
      _controllerList.add(_controller);
    }
  }

}

_copyToClipboard(String url, BuildContext context) {
  Clipboard.setData(new ClipboardData(text: url));
  Scaffold.of(context).showSnackBar(SnackBar(content: Text(url)));
}


